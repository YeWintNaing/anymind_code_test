CREATE schema If Not EXISTS anymind;

CREATE TABLE bis_wlt_wallet
(
    wallet_id    int8    NOT NULL,
    "name"       varchar NULL,
    balance      numeric NOT NULL,
    added_on     int8 NULL,
    created_date int8 NULL,
    updated_date int8 NULL,
    "version"    int4 NULL,
    CONSTRAINT bis_wlt_wallet_pk PRIMARY KEY (wallet_id),
    CONSTRAINT bis_wlt_wallet_un UNIQUE (wallet_id)
);

-- Permissions

ALTER TABLE bis_wlt_wallet OWNER TO postgres;
GRANT
ALL
ON TABLE bis_wlt_wallet TO postgres;



CREATE TABLE bis_wlt_balance_ledger
(
    balance_ledger_id  int8 NOT NULL,
    wallet_id          int8 NOT NULL,
    "before"           numeric NULL,
    "after"            numeric NULL,
    transaction_amount numeric NULL,
    transaction_date   int8 NULL,
    description        varchar NULL,
    "version"          int4 NULL,
    created_date       int8 NULL,
    updated_date       int8 NULL,
    CONSTRAINT bis_wlt_balance_ledger_pk PRIMARY KEY (balance_ledger_id),
    CONSTRAINT bis_wlt_balance_ledger_un UNIQUE (balance_ledger_id)
);

-- Permissions

ALTER TABLE bis_wlt_balance_ledger OWNER TO postgres;
GRANT
ALL
ON TABLE bis_wlt_balance_ledger TO postgres;


-- mopayments.bis_wlt_balance_ledger foreign keys

ALTER TABLE bis_wlt_balance_ledger
    ADD CONSTRAINT bis_wlt_balance_ledger_fk FOREIGN KEY (wallet_id) REFERENCES bis_wlt_wallet (wallet_id) ON DELETE CASCADE ON UPDATE CASCADE;

