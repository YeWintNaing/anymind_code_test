package com.ywn.anymindtest.wallet.exception

import com.ywn.anymindtest.common.exception.DomainException
import com.ywn.anymindtest.common.exception.ExceptionMessage
import com.ywn.anymindtest.common.exception.LanguageType


class WalletNotFoundException : DomainException() {
    override fun asMessage(): ExceptionMessage {
        val exceptionMessage = ExceptionMessage.of(this)
        exceptionMessage.addText(LanguageType.ENGLISH, "Wallet not found.")
        return exceptionMessage
    }


}