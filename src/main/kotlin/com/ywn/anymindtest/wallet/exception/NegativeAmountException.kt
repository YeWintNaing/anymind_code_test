package com.ywn.anymindtest.wallet.exception

import com.ywn.anymindtest.common.exception.DomainException
import com.ywn.anymindtest.common.exception.ExceptionMessage
import com.ywn.anymindtest.common.exception.LanguageType


class NegativeAmountException : DomainException() {

    override fun asMessage(): ExceptionMessage {
        val exceptionMessage = ExceptionMessage.of(this)
        exceptionMessage.addText(LanguageType.ENGLISH, "Negative amount.")
        return exceptionMessage
    }


}