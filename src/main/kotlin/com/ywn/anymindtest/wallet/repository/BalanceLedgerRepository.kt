package com.ywn.anymindtest.wallet.repository

import com.ywn.anymindtest.wallet.model.BalanceLedger
import com.ywn.anymindtest.wallet.model.BalanceLedgerId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor
import org.springframework.stereotype.Repository


@Repository
interface BalanceLedgerRepository : JpaRepository<BalanceLedger, BalanceLedgerId>,
    QuerydslPredicateExecutor<BalanceLedger>