package com.ywn.anymindtest.wallet.repository.filters

import com.querydsl.core.types.dsl.BooleanExpression
import com.ywn.anymindtest.wallet.model.QWallet
import com.ywn.anymindtest.wallet.model.WalletId


object WalletFilters {


    fun withName(name: String): BooleanExpression {
        return QWallet.wallet.name.eq(name)
    }

    fun withId(walletId: WalletId): BooleanExpression {
        return QWallet.wallet.walletId.eq(walletId)
    }

}