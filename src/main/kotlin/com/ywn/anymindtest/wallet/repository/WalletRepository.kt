package com.ywn.anymindtest.wallet.repository

import com.ywn.anymindtest.wallet.model.Wallet
import com.ywn.anymindtest.wallet.model.WalletId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Lock
import org.springframework.data.jpa.repository.Query
import org.springframework.data.querydsl.QuerydslPredicateExecutor
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*
import javax.persistence.LockModeType


@Repository
interface WalletRepository : JpaRepository<Wallet, WalletId>, QuerydslPredicateExecutor<Wallet> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query(value = "SELECT e FROM Wallet e WHERE e.walletId = :walletId")
    fun selectForUpdate(@Param("walletId") walletId: WalletId?): Optional<Wallet>


}