package com.ywn.anymindtest.wallet.repository.filters

import com.querydsl.core.types.dsl.BooleanExpression
import com.ywn.anymindtest.wallet.model.BalanceLedgerId
import com.ywn.anymindtest.wallet.model.QBalanceLedger
import com.ywn.anymindtest.wallet.model.WalletId
import java.time.Instant


object BalanceLedgerFilters {

    fun notWithBalanceLedgerId(balanceLedgerId: BalanceLedgerId): BooleanExpression {
        return QBalanceLedger.balanceLedger.balanceLedgerId.ne(balanceLedgerId)
    }

    fun withWalletId(walletId: WalletId): BooleanExpression {
        return QBalanceLedger.balanceLedger.wallet.walletId.eq(walletId)
    }

    fun withStartingFrom(from: Instant): BooleanExpression {
        return QBalanceLedger.balanceLedger.transactionDate.goe(from)
    }

    fun withUntil(to: Instant): BooleanExpression {
        return QBalanceLedger.balanceLedger.transactionDate.loe(to)
    }


}