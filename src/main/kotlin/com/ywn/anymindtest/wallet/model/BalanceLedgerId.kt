package com.ywn.anymindtest.wallet.model

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class BalanceLedgerId(
    @Column(name = "balance_ledger_id") val id: Long
) : Serializable