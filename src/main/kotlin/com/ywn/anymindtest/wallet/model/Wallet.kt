package com.ywn.anymindtest.wallet.model

import com.ywn.anymindtest.common.Snowflake
import com.ywn.anymindtest.common.model.JpaEntity
import com.ywn.anymindtest.common.model.JpaInstantConverter
import com.ywn.anymindtest.wallet.exception.NegativeAmountException
import java.math.BigDecimal
import java.time.Instant
import javax.persistence.*

@Entity
@Table(name = "bis_wlt_wallet")
class Wallet(
    @EmbeddedId val walletId: WalletId = WalletId(Snowflake.get().nextId()),
    @Column(name = "name") val name: String,
    @Column(name = "balance") var balance: BigDecimal,
    @Column(name = "added_on") @Convert(converter = JpaInstantConverter::class) val addedOn: Instant = Instant.now()
) : JpaEntity() {

    @Throws(NegativeAmountException::class)
    fun place(amount: BigDecimal, datetime: Instant, description: String): BalanceLedger {
        if (amount.toLong() < 0) {
            throw NegativeAmountException()
        }
        val before = balance
        balance = balance.add(amount)
        return BalanceLedger(
            before = before,
            transactionAmount = amount,
            after = balance,
            description = description,
            wallet = this,
            transactionDate = datetime
        )
    }


}