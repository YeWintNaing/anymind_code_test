package com.ywn.anymindtest.wallet.model

import com.ywn.anymindtest.common.Snowflake
import com.ywn.anymindtest.common.model.JpaEntity
import com.ywn.anymindtest.common.model.JpaInstantConverter
import java.math.BigDecimal
import java.time.Instant
import javax.persistence.*

@Entity
@Table(name = "bis_wlt_balance_ledger")
class BalanceLedger(

    @EmbeddedId
    val balanceLedgerId: BalanceLedgerId = BalanceLedgerId(Snowflake.get().nextId()),

    @Column(name = "before")
    val before: BigDecimal,

    @Column(name = "transaction_amount")
    val transactionAmount: BigDecimal,

    @Column(name = "after")
    val after: BigDecimal,

    @Column(name = "transaction_date")
    @Convert(converter = JpaInstantConverter::class)
    val transactionDate: Instant = Instant.now(),

    @Column(name = "description")
    val description: String,

    @ManyToOne
    @JoinColumn(name = "wallet_id")
    val wallet: Wallet

) : JpaEntity()