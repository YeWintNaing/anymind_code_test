package com.ywn.anymindtest.wallet.model

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class WalletId(
    @Column(name = "wallet_id") val id: Long
) : Serializable