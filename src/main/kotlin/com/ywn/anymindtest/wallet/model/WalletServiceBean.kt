package com.ywn.anymindtest.wallet.model

import com.querydsl.core.BooleanBuilder
import com.ywn.anymindtest.common.PagedRequest
import com.ywn.anymindtest.common.PagedResult
import com.ywn.anymindtest.common.toIsoString
import com.ywn.anymindtest.wallet.exception.WalletNotFoundException
import com.ywn.anymindtest.wallet.repository.BalanceLedgerRepository
import com.ywn.anymindtest.wallet.repository.WalletRepository
import com.ywn.anymindtest.wallet.repository.filters.BalanceLedgerFilters.withStartingFrom
import com.ywn.anymindtest.wallet.repository.filters.BalanceLedgerFilters.withUntil
import com.ywn.anymindtest.wallet.repository.filters.BalanceLedgerFilters.withWalletId
import com.ywn.anymindtest.wallet.repository.filters.WalletFilters
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.time.Instant


@Service
class WalletServiceBean(
    val walletRepository: WalletRepository, val balanceLedgerRepository: BalanceLedgerRepository
) : WalletService {

    @Transactional
    @Throws(WalletNotFoundException::class)
    override fun get(walletId: WalletId): Wallet {

        return walletRepository.findOne(WalletFilters.withId(walletId)).orElseThrow {
            WalletNotFoundException()
        }

    }

    @Transactional
    override fun getBalanceLedgerHistory(
        from: Instant?, to: Instant?, walletId: WalletId?, pagedRequest: PagedRequest
    ): PagedResult<BalanceLedger> {
        val builder = BooleanBuilder()

        if (from != null && to != null) {
            builder.and(withStartingFrom(from).and(withUntil(to)))
        }


        if (walletId != null) {
            builder.and(withWalletId(walletId))
        }

        val pageable: Pageable = PageRequest.of(
            pagedRequest.pageSize, pagedRequest.offset, Sort.by("transactionDate").descending()
        )

        val page: Page<BalanceLedger> = this.balanceLedgerRepository.findAll(builder, pageable)

        return PagedResult(
            page.content, pagedRequest.offset, pagedRequest.pageSize, page.totalElements.toInt()
        )
    }

    @Transactional(
        propagation = Propagation.REQUIRES_NEW, rollbackFor = [Exception::class]
    )
    override fun sendBitCoins(walletId: WalletId, dateTime: Instant, amount: BigDecimal) {
        val wallet: Wallet = walletRepository.selectForUpdate(walletId)
            .orElseThrow { WalletNotFoundException() }

        val balanceLedger = wallet.place(
            amount,
            dateTime,
            "Send BitCoins at ${dateTime.toIsoString()}"
        )

        walletRepository.save(wallet)
        balanceLedgerRepository.save(balanceLedger)
    }
}