package com.ywn.anymindtest.wallet.model

import com.ywn.anymindtest.common.PagedRequest
import com.ywn.anymindtest.common.PagedResult
import com.ywn.anymindtest.wallet.exception.NegativeAmountException
import com.ywn.anymindtest.wallet.exception.WalletNotFoundException
import java.math.BigDecimal
import java.time.Instant


interface WalletService {

    @Throws(WalletNotFoundException::class)
    fun get(walletId: WalletId): Wallet

    fun getBalanceLedgerHistory(
        from: Instant?, to: Instant?, walletId: WalletId?, pagedRequest: PagedRequest
    ): PagedResult<BalanceLedger>

    @Throws(WalletNotFoundException::class, NegativeAmountException::class)
    fun sendBitCoins(walletId: WalletId, dateTime: Instant, amount: BigDecimal)


}