package com.ywn.anymindtest.config

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.*
import org.springframework.core.env.Environment
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.annotation.EnableTransactionManagement
import java.util.*
import javax.persistence.EntityManagerFactory
import javax.sql.DataSource

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    basePackages = ["com.ywn.anymindtest"],
    excludeFilters = [ComponentScan.Filter(type = FilterType.ANNOTATION, classes = [])],
    entityManagerFactoryRef = "entityManagerFactory",
    considerNestedRepositories = true
)
@PropertySource("classpath:/postgres_ds.properties")
class PostgresConfiguration {
    @Autowired
    private val env: Environment? = null
    @Bean(destroyMethod = "close")
    @Primary
    fun dataSource(): DataSource {
        val config = HikariConfig()
        config.jdbcUrl = env!!.getRequiredProperty("jdbc.db.url")
        config.username = env.getRequiredProperty("jdbc.db.username")
        config.password = env.getRequiredProperty("jdbc.db.password")
        config.driverClassName = env.getRequiredProperty("jdbc.db.driver")
        config.addDataSourceProperty("cachePrepStmts", "true")
        config.addDataSourceProperty("prepStmtCacheSize", "250")
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048")
        config.addDataSourceProperty("useServerPrepStmts", true)
        config.addDataSourceProperty("useLocalSessionState", true)
        config.addDataSourceProperty("rewriteBatchedStatements", true)
        config.addDataSourceProperty("cacheResultSetMetadata", true)
        config.addDataSourceProperty("cacheServerConfiguration", true)
        config.addDataSourceProperty("elideSetAutoCommits", true)
        config.addDataSourceProperty("maintainTimeStats", false)
        config.maximumPoolSize = env.getRequiredProperty("jdbc.db.poolSize").toInt()
        return HikariDataSource(config)
    }

    @Bean
    @Primary
    fun entityManagerFactory(dataSource: DataSource): LocalContainerEntityManagerFactoryBean {
        val entityManagerFactoryBean = LocalContainerEntityManagerFactoryBean()
        entityManagerFactoryBean.dataSource = dataSource
        entityManagerFactoryBean.jpaVendorAdapter = HibernateJpaVendorAdapter()
        entityManagerFactoryBean.setPackagesToScan("com.ywn.anymindtest.**")
        val jpaProperties = Properties()
        jpaProperties["hibernate.dialect"] = env!!.getRequiredProperty("jdbc.hibernate.dialect")
        jpaProperties["hibernate.show_sql"] = env.getRequiredProperty("jdbc.hibernate.show_sql")
        jpaProperties["hibernate.format_sql"] = env.getRequiredProperty("jdbc.hibernate.format_sql")

//        jpaProperties["hibernate.default_schema"] = env.getRequiredProperty("jdbc.hibernate.default_schema")
        entityManagerFactoryBean.setJpaProperties(jpaProperties)
        return entityManagerFactoryBean
    }

    @Bean
    @Primary
    fun transactionManager(entityManagerFactory: EntityManagerFactory?): JpaTransactionManager {
        val transactionManager = JpaTransactionManager()
        transactionManager.entityManagerFactory = entityManagerFactory
        return transactionManager
    }
}