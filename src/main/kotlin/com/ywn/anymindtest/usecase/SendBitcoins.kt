package com.ywn.anymindtest.usecase

import com.ywn.anymindtest.common.exception.DomainException
import java.math.BigDecimal
import java.time.Instant


interface SendBitcoins {

    data class Input(val amount: BigDecimal, val dateTime: Instant)

    data class Output(val result: String)

    @Throws(DomainException::class)
    fun execute(input: Input): Output

}