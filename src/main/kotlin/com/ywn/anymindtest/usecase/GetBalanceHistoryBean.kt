package com.ywn.anymindtest.usecase

import com.ywn.anymindtest.common.PagedRequest
import com.ywn.anymindtest.common.PagedResult
import com.ywn.anymindtest.usecase.GetBalanceHistory.BalanceLedgerDetail
import com.ywn.anymindtest.wallet.model.BalanceLedger
import com.ywn.anymindtest.wallet.model.WalletId
import com.ywn.anymindtest.wallet.model.WalletService
import org.springframework.stereotype.Service
import java.util.stream.Collectors


@Service
class GetBalanceHistoryBean(
    val walletService: WalletService
) : GetBalanceHistory {
    override fun execute(input: GetBalanceHistory.Input): GetBalanceHistory.Output {

        val (result, offset, pageSize, count) = walletService.getBalanceLedgerHistory(
            input.from, input.to, WalletId(271995640832937984L), PagedRequest(
                0, Int.MAX_VALUE
            )
        )

        val data = result.map { detail: BalanceLedger ->
            BalanceLedgerDetail(
                detail.balanceLedgerId,
                detail.before,
                detail.after,
                detail.transactionAmount,
                detail.description,
                detail.transactionDate
            )
        }

        val pagedResult = PagedResult(
            data, offset, pageSize, count
    )

        return GetBalanceHistory.Output(pagedResult)


    }
}