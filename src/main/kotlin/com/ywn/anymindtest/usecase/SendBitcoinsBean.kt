package com.ywn.anymindtest.usecase

import com.ywn.anymindtest.wallet.model.WalletId
import com.ywn.anymindtest.wallet.model.WalletService
import org.springframework.stereotype.Service

@Service
class SendBitcoinsBean(
    val walletService: WalletService
) : SendBitcoins {


    override fun execute(input: SendBitcoins.Input): SendBitcoins.Output {
        this.walletService.sendBitCoins(WalletId(271995640832937984L), input.dateTime, input.amount)

        return SendBitcoins.Output("Success")
    }
}