package com.ywn.anymindtest.usecase

import com.ywn.anymindtest.common.PagedResult
import com.ywn.anymindtest.common.exception.DomainException
import com.ywn.anymindtest.wallet.model.BalanceLedgerId
import java.math.BigDecimal
import java.time.Instant


interface GetBalanceHistory {
    data class Output(
        var pagedResult: PagedResult<BalanceLedgerDetail>? = null
    )


    data class BalanceLedgerDetail(
        val balanceLedgerId: BalanceLedgerId,
        val before: BigDecimal,
        val after: BigDecimal,
        val transactionAmount: BigDecimal,
        val description: String,
        val transactionDate: Instant
    )


    data class Input(
        val from: Instant? = null,
        val to: Instant? = null
    )

    @Throws(DomainException::class)
    fun execute(input: Input): Output

}