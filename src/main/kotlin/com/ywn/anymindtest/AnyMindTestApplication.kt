package com.ywn.anymindtest

import com.ywn.anymindtest.config.PostgresConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Import

@SpringBootApplication
class AnyMindTestApplication

fun main(args: Array<String>) {
    runApplication<AnyMindTestApplication>(*args)
}
