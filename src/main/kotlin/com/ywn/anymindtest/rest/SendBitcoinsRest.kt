package com.ywn.anymindtest.rest

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.ywn.anymindtest.common.exception.DomainException
import com.ywn.anymindtest.common.rest.converter.StringToInstant
import com.ywn.anymindtest.usecase.SendBitcoins
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.math.BigDecimal
import java.time.Instant
import javax.validation.Valid
import javax.validation.constraints.Min


@RestController
class SendBitcoinsRest(
    val sendBitcoins: SendBitcoins
) {

    data class Request @JsonCreator constructor(
        @JsonProperty("datetime")
        @JsonDeserialize(using = StringToInstant::class)
        val datetime: Instant,
        @JsonProperty("amount")
        @field:Min(value = 1,  message =  "Minimum value must be 1.")
        val amount: BigDecimal
    )

    data class Response(

        @JsonProperty("result")
        val result: String


    )

    @PostMapping("/wallet/send_bitcoins")
    @Throws(
        DomainException::class
    )
    fun execute(@RequestBody @Valid request:  Request): ResponseEntity<Response> {
        LOG.info("Request : [{}]", request)

        val output = this.sendBitcoins.execute(SendBitcoins.Input(request.amount, request.datetime))


        return ResponseEntity(
            Response(output.result),
            HttpStatus.OK
        )
    }

    companion object {
        private val LOG: Logger = LoggerFactory.getLogger(SendBitcoinsRest::class.java)
    }


}