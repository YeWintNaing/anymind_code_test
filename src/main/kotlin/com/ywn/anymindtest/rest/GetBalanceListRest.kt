package com.ywn.anymindtest.rest

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.sun.istack.NotNull
import com.ywn.anymindtest.common.exception.DomainException
import com.ywn.anymindtest.common.rest.converter.StringToInstant
import com.ywn.anymindtest.common.toIsoString
import com.ywn.anymindtest.usecase.GetBalanceHistory
import com.ywn.anymindtest.usecase.GetBalanceHistory.BalanceLedgerDetail
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.time.Instant
import javax.validation.Valid
import javax.validation.constraints.NotBlank


@RestController
class GetBalanceListRest(
    val getBalanceHistory: GetBalanceHistory
) {

    data class Request @JsonCreator constructor(
        @JsonProperty("startDateTime") @JsonDeserialize(using = StringToInstant::class) @field:NotNull  val startDateTime: Instant,
        @JsonProperty("endDateTime") @JsonDeserialize(using = StringToInstant::class) @field:NotNull  val endDateTime: Instant
    )

    data class Response(
//        @JsonProperty("balance_ledger_id")
//        val balanceLedgerId: String,
//        @JsonProperty("before")
//        val before: BigDecimal,
//
//        @JsonProperty("after")
//        val after: BigDecimal,

        @JsonProperty("amount") val transactionAmount: BigDecimal,


        @JsonProperty("datetime") val transactionDate: String,

//        @JsonProperty("description")
//        val description: String
    )

    @PostMapping("/wallet/get_balance_history")
    @Throws(
        DomainException::class
    )
    fun execute(@RequestBody request: @Valid Request): ResponseEntity<List<Response>> {

        val output = this.getBalanceHistory.execute(
                GetBalanceHistory.Input(

                    request.startDateTime, request.endDateTime
                ),
            )


        val balanceLedgerList: List<Response> = output.pagedResult?.result?.map { data: BalanceLedgerDetail ->
            Response(
                data.transactionAmount,
                data.transactionDate.toIsoString(),
            )
        } ?: listOf()

        return ResponseEntity(
            balanceLedgerList, HttpStatus.OK
        )
    }
}