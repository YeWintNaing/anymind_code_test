package com.ywn.anymindtest.common

import java.net.NetworkInterface
import java.security.SecureRandom
import java.time.Instant
import kotlin.math.pow

class Snowflake {
    private val TOTAL_BITS = 64

    private val EPOCH_BITS = 42

    private val NODE_ID_BITS = 10

    private val SEQUENCE_BITS = 12

    private val maxNodeId = (2.0.pow(NODE_ID_BITS.toDouble()) - 1).toInt()

    private val maxSequence = (2.0.pow(SEQUENCE_BITS.toDouble()) - 1).toInt()

    private val CUSTOM_EPOCH = 1577813400000L


    companion object {
        private val INSTANCE: Snowflake = Snowflake()

        fun get(): Snowflake {
            return INSTANCE
        }
    }


    private fun timestamp(): Long {
        return Instant.now().toEpochMilli() - CUSTOM_EPOCH
    }

    private var nodeId = 0

    private var lastTimestamp = -1L

    private var sequence = 0L

    init {
        nodeId = createNodeId()
    }

    private fun createNodeId(): Int {
        var nodeId: Int = try {
            val sb = StringBuilder()
            val networkInterfaces = NetworkInterface.getNetworkInterfaces()
            while (networkInterfaces.hasMoreElements()) {
                val networkInterface = networkInterfaces.nextElement()
                val mac = networkInterface.hardwareAddress
                if (mac != null) {
                    for (i in mac.indices) {
                        sb.append(String.format("%02X", mac[i]))
                    }
                }
            }
            sb.toString().hashCode()
        } catch (ex: Exception) {
            SecureRandom().nextInt()
        }
        nodeId = nodeId and maxNodeId
        return nodeId
    }

    fun nextId(): Long {
        var currentTimestamp = timestamp()
        synchronized(this) {
            check(currentTimestamp >= lastTimestamp) { "Invalid System Clock!" }
            if (currentTimestamp == lastTimestamp) {
                sequence = sequence + 1 and maxSequence.toLong()
                if (sequence == 0L) {
                    currentTimestamp = waitNextMillis(currentTimestamp)
                }
            } else {
                sequence = 0
            }
            lastTimestamp = currentTimestamp
        }
        var id = currentTimestamp shl TOTAL_BITS - EPOCH_BITS
        id = id or (nodeId shl TOTAL_BITS - EPOCH_BITS - NODE_ID_BITS).toLong()
        id = id or sequence
        return id
    }

    private fun waitNextMillis(currentTimestamp: Long): Long {
        var currentTimestamp = currentTimestamp
        while (currentTimestamp == lastTimestamp) {
            currentTimestamp = timestamp()
        }
        return currentTimestamp
    }
}