package com.ywn.anymindtest.common.rest.converter

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.ywn.anymindtest.common.toIsoString
import java.io.IOException
import java.math.BigDecimal
import java.time.Instant


class InstantToString : JsonSerializer<Instant>() {
    @Throws(IOException::class)
    override fun serialize(value: Instant, gen: JsonGenerator, serializers: SerializerProvider) {
        gen.writeString(value.toIsoString())
    }
}