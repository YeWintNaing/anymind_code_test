package com.ywn.anymindtest.common.rest

import com.ywn.anymindtest.common.exception.DomainException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import javax.persistence.EntityNotFoundException
import javax.validation.ValidationException


@RestControllerAdvice
class RestExceptionHandler {
    private fun errorResponse(exception: Exception): ErrorEntity {
        return ErrorEntityUtil.buildErrorEntity(exception)
    }

    @ExceptionHandler(value = [DomainException::class])
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    fun processDomainExceptionException(ex: DomainException): ErrorEntity {
        LOG.error("ERROR : ", ex)
        return errorResponse(ex)
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    fun processNotFound(): ErrorEntity {
        return errorResponse(Exception("NOT_FOUND"))
    }

    @ExceptionHandler(value = [EntityNotFoundException::class, RuntimeException::class])
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    fun processRuntimeException(ex: RuntimeException): ErrorEntity {
        LOG.error("ERROR : ", ex)
        return errorResponse(ex)
    }

    @ExceptionHandler(value = [MethodArgumentNotValidException::class, ValidationException::class])
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    fun processValidationError(ex: MethodArgumentNotValidException): ErrorEntity {
        LOG.error("ERROR : ", ex)
        return errorResponse(ex)
    }

    companion object {
        private val LOG: Logger = LoggerFactory.getLogger(RestExceptionHandler::class.java)
    }
}