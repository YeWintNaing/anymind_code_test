package com.ywn.anymindtest.common.rest

import com.ywn.anymindtest.common.exception.DomainException
import com.ywn.anymindtest.common.exception.LanguageType
import com.ywn.anymindtest.common.exception.Text
import org.springframework.web.bind.MethodArgumentNotValidException
import java.util.function.Consumer
import javax.validation.ValidationException


object ErrorEntityUtil {
    fun buildErrorEntity(exception: Exception): ErrorEntity {
        return when (exception) {
            is DomainException -> {
                val exceptionMessage = exception.asMessage()
                val errorEntity = ErrorEntity.of(exception.asCode())
                if (exceptionMessage != null) {
                    exceptionMessage.texts.forEach(Consumer { text: Text? ->
                        errorEntity.addMessage(
                            text!!
                        )
                    })
                } else {
                    errorEntity
                        .addMessage(Text(LanguageType.ENGLISH, "Problem occurred while processing your request."))
                }
                errorEntity
            }
            is MethodArgumentNotValidException, is ValidationException -> {
                val errorEntity = ErrorEntity.of("INPUT_EXCEPTION")
                errorEntity.addMessage(
                    Text(LanguageType.ENGLISH, "Problem in data inputs. Error : " + exception.message)
                )
                errorEntity
            }
            else -> {
                val errorEntity = ErrorEntity.of("UNKNOWN_EXCEPTION")
                errorEntity.addMessage(
                    Text(
                        LanguageType.ENGLISH,
                        "Something went wrong. Please try again. Possible error : " + exception.message
                    )
                )
                errorEntity
            }
        }
    }
}