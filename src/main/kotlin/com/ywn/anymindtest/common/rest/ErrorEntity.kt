package com.ywn.anymindtest.common.rest

import com.fasterxml.jackson.annotation.JsonProperty
import com.ywn.anymindtest.common.exception.Text


class ErrorEntity {
    @JsonProperty("error_code")
    private var errorCode: String? = null

    @JsonProperty("messages")
    private val messages: MutableSet<Text> = HashSet()
    fun addMessage(message: Text) {
        messages.add(message)
    }

    companion object {
        fun of(errorCode: String?): ErrorEntity {
            val entity = ErrorEntity()
            entity.errorCode = errorCode
            return entity
        }
    }
}
