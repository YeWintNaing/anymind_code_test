package com.ywn.anymindtest.common.rest.converter

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.io.IOException
import java.time.Instant
import java.time.ZonedDateTime





class StringToInstant : JsonDeserializer<Instant?>() {
    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Instant? {
        val timestamp  = p.valueAsString ?: return null

        val zonedDateTime = ZonedDateTime.parse(timestamp)


        return  zonedDateTime.toInstant()
    }
}
