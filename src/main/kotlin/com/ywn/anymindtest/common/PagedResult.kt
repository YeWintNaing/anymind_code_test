package com.ywn.anymindtest.common


data class PagedResult<T>(val result: List<T>, val offset: Int, val pageSize: Int, val count: Int)