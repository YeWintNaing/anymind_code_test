package com.ywn.anymindtest.common


data class PagedRequest(val pageSize: Int, val offset: Int)
