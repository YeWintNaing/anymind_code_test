package com.ywn.anymindtest.common

import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


fun Instant.toIsoString(): String {
    val formatter: DateTimeFormatter = DateTimeFormatter.ISO_ZONED_DATE_TIME

    val timeZone = ZoneId.systemDefault()

    val zonedDateTime: ZonedDateTime = this.atZone(ZoneId.of(timeZone.id))


    return zonedDateTime.format(formatter)

}