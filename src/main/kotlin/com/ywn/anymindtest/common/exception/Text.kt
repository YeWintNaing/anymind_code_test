package com.ywn.anymindtest.common.exception

data class Text(
    val language: LanguageType,
    val description: String
)
