package com.ywn.anymindtest.common.exception

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*


open class DomainException : Exception {
    private var params: List<String>? = null

    constructor() : super()
    constructor(params: List<String>?) {
        this.params = params
    }

    constructor(throwable: Throwable?) : super(throwable)

    fun asCode(): String {
        val exceptionName = this.javaClass.simpleName
        return exceptionName.replace(
            String.format(
                "%s|%s|%s", "(?<=[A-Z])(?=[A-Z][a-z])", "(?<=[^A-Z])(?=[A-Z])",
                "(?<=[A-Za-z])(?=[^A-Za-z])"
            ).toRegex(), " "
        ).replace(" ".toRegex(), "_").uppercase(Locale.getDefault())
    }

    open fun asMessage(): ExceptionMessage? {
        LOG.info("asMessage >> start and return optional empty")
        return null
    }

    companion object {
        private val LOG: Logger = LoggerFactory.getLogger(DomainException::class.java)

    }
}
