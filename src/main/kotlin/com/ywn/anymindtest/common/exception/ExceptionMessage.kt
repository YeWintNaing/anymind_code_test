package com.ywn.anymindtest.common.exception


class ExceptionMessage {
    var exception: Exception? = null
    var code: String? = null
    var texts: MutableSet<Text> = HashSet()
    fun addText(language: LanguageType, description: String) {
        texts.add(Text(language, description))
    }

    fun getText(language: LanguageType): Text? {
        return texts.firstOrNull { text: Text ->
            text.language == language
        }
    }

    companion object {
        fun of(e: DomainException): ExceptionMessage {
            val exceptionMessage = ExceptionMessage()
            exceptionMessage.exception = e
            exceptionMessage.code = e.asCode()
            return exceptionMessage
        }
    }
}