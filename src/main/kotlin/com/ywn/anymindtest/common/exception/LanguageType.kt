package com.ywn.anymindtest.common.exception

enum class LanguageType {
    MYANMAR, ENGLISH
}