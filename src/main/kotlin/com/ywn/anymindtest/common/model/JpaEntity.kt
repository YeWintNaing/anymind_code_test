package com.ywn.anymindtest.common.model

import org.springframework.data.domain.Persistable
import java.time.Instant
import javax.persistence.*


@MappedSuperclass
abstract class JpaEntity {

    @Column(name = "created_date", updatable = false)
    @Convert(converter = JpaInstantConverter::class)
    protected var createdDate: Instant? = null

    @Column(name = "updated_date")
    @Convert(converter = JpaInstantConverter::class)
    protected var updatedDate: Instant? = null

    @Column(name = "version")
    @Version
    protected var version: Int? = null

    @PrePersist
    fun prePersist() {
        createdDate = Instant.now()
        updatedDate = Instant.now()
    }

    @PreUpdate
    fun preUpdate() {
        updatedDate = Instant.now()
    }


//    override fun toString(): String {
//        return "BaseEntity(id=$id, version=$version, createdAt=$createdDate, updatedAt=$updatedDate, isNew=$isNew)"
//    }
//
//    override fun equals(other: Any?): Boolean {
//        if (this === other) return true
//        if (javaClass != other?.javaClass) return false
//        other as JpaEntity<*>
//        if (id != other.id) return false
//        return true
//    }
//
//    override fun hashCode(): Int {
//        return id?.hashCode() ?: 0
//    }
}