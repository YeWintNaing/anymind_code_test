package com.ywn.anymindtest.common.model

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Instant

import javax.persistence.AttributeConverter
import javax.persistence.Converter


@Converter
class JpaInstantConverter : AttributeConverter<Instant?, Long?> {
    override fun convertToDatabaseColumn(attribute: Instant?): Long? {
        LOG.debug(" JpaInstantConverter : attribute [{}]: ", attribute)
        return attribute?.toEpochMilli()
    }

    override fun convertToEntityAttribute(dbData: Long?): Instant? {
        return if (dbData == null) null else Instant.ofEpochMilli(dbData)
    }

    companion object {
        private val LOG: Logger = LoggerFactory.getLogger(JpaInstantConverter::class.java)
    }
}